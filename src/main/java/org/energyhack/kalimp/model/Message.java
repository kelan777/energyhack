package org.energyhack.kalimp.model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.time.temporal.ChronoField;

@Document("messages")
public class Message {

    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String textMsg;
    private long timestamp;
    private String nameSender;
    private String status;
    private int priority;

    public Message(){}

    public Message(String phone, String textMsg, String nameSender) {
        this.phone = phone;
        this.textMsg = textMsg;
        /**
         * ??????????????????????????????????????????
         */
        this.timestamp = Instant.now().getLong(ChronoField.INSTANT_SECONDS);

        this.nameSender = nameSender;
    }

    public String getTextMsg() {
        return textMsg;
    }

    public void setTextMsg(String textMsg) {
        this.textMsg = textMsg;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getNameSender() {
        return nameSender;
    }

    public void setNameSender(String nameSender) {
        this.nameSender = nameSender;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
