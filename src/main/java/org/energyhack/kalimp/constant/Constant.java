package org.energyhack.kalimp.constant;

public interface Constant {

	String HEADER_SECURITY_TOKEN = "X-AuthToken";

	String DEVICE_PATH = "/devices/**";
	String AUTH_PATH = "/auth/**";

	String AUTH_SUCCESS = "/auth/success";
	String AUTH_FAIL = "/auth/failure";

	String SUCCESS_STATUS = "success";
	String FAILED_STATUS = "fail";
}
