package org.energyhack.kalimp.repository;

import org.energyhack.kalimp.model.Message;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MsgRepository extends MongoRepository<Message, String> {
    public Message findByPhone();
}
