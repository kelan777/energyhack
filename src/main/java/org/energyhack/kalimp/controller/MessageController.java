package org.energyhack.kalimp.controller;

import org.energyhack.kalimp.model.Message;
import org.energyhack.kalimp.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.energyhack.kalimp.repository.MsgRepository;

import java.util.List;


@RestController
@RequestMapping({"/messages"})
public class MessageController {

    @Autowired
    private MsgRepository repository;

    @Autowired
    private SmsService smsService;

    MessageController(MsgRepository contactRepository) {
        this.repository = contactRepository;
    }

    @GetMapping(path = {"/send"})
    public String sendMessage() {
        //Сортировка по приоритету
        smsService.sendSms();

        System.out.println("Message from Aleha");
        return "Cообщение доставлено";
    }

    @GetMapping
    public List<Message> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = {"/{id}"})
    public ResponseEntity<Message> findById(@PathVariable String id) {
        return repository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }


    @Bean
    public SmsService getSmsService() {
        return new SmsService();
    }
}